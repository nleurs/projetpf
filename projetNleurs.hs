import System.IO
import System.Random
import System.IO.Unsafe

type Point = (Double,Double)

type Dimension = (Double,Double)

type Position = (Double,Double)

type Crayon = Bool

data Shape = Empty | Line Point Point | Rectangle Point Dimension | Cercle  Point Int | Polyline [Point]
          deriving (Show)

data Canvas = Canvas Dimension [Shape]
          deriving (Show)

data Tortue = Tortue Position Double Crayon
          deriving (Show)

data World = World Tortue [Shape]
          deriving (Show)

printPoint p = show (fst p) ++ "," ++ show (snd p) ++ " "

buildShape (Line x y)= "<line x1='"++ show (fst x) ++"' y1='"++show (snd x)++"' x2='"++ show (fst y)++"' y2='"++show (snd y)++"' style='stroke:rgb(0,0,0);stroke-width:1'></line>"
buildShape (Rectangle x d)= "<rect x='"++show (fst x) ++"' y='"++show (snd x) ++"' width='"++show (fst d)++"' height='"++show (snd d)++"'style='stroke:rgb(0,0,0);stroke-width:1'></rect>"
buildShape (Cercle x ray) = "<circle cx='"++ show (fst x)++"' cy='"++show (snd x) ++"' r='"++ show ray++"'style='stroke:rgb(0,0,0);stroke-width:1'></circle>"
buildShape (Polyline points) = "<polyline fill='none' points='" ++ concat (map printPoint points) ++"'style='stroke:rgb(0,0,0);stroke-width:1'></polyline>"

buildSvg (Canvas d shapes) = "<svg height='"++show (fst d)++"' width='"++show (snd d) ++"'>"
                             ++ concat (map buildShape shapes)
                             ++ "</svg>"

randomSquare = do
      x <- randomRIO(0,500)
      y <- randomRIO(0,500)
      h <- randomRIO(0,500)
      w <- randomRIO(0,500)
      return (Rectangle(x,y)(h,w))

printTortue (Tortue x a c) = "La tortue ce situe aux coordonnee " ++ printPoint x ++ " avec un angle de "++ show a ++ " et le crayon " ++ show c

degToRad deg = deg * pi / 180

avancer (Tortue x a c) distance = Tortue (newX,newY) a c
                         where newX = ((fst x) + (cos (degToRad a)) * distance)
                               newY = snd x + (sin (degToRad a)) * distance

reculer (Tortue x a c) distance = Tortue (newX,newY) a c
                         where newX = ((fst x) - (cos (degToRad a)) * distance)
                               newY = snd x - (sin (degToRad a)) * distance

tournerGauche (Tortue x a c) deg = Tortue x (a-deg) c

tournerDroite (Tortue x a c) deg = Tortue x (a+deg) c

lever (Tortue x a c) = Tortue x a False

baisser (Tortue x a c) = Tortue x a True

dessiner (Tortue x1 _ _) (Tortue x2 _ c) | c == True = Line x1 x2
                                         | otherwise = Line (0.0,0.0) (0.0,0.0)

reset (Tortue x a c) width height = Tortue (width / 2,height / 2) 0 True

av (World t shapes) distance = World (avancer t distance) (dessiner t (avancer t distance) : shapes)

rc (World t shapes) distance = World (avancer t distance) (dessiner t (avancer t distance) : shapes)

td (World t shapes) deg = World (tournerDroite t deg) shapes

tg (World t shapes) deg = World (tournerGauche t deg) shapes

getShapes (World t shapes) = shapes

getTortue (World t shapes) = t

addShape (World t shapes) shape = World t (shape : shapes)

--drawSquare world = take 4 (repeat (av world 20))

--drawSquare world distance x | x == 4 = return
--drawSquare world distance x | mod x 2 == 0 = drawSquare (av world distance) distance (x+1)
--drawSquare world distance x | otherwise = drawSquare (av world distance) distance (x+1)
--drawSquare world distance = (av world distance) (td world 90)



main = do
    let width = 500.0
    let height = 500.0

    --génération Tortue

    let t = Tortue (width / 2,height / 2) 0 True
    let w = World t []

{-
    let distance = 50

    let w1 = av w distance
    let w2 = td w1 90
    let w3 = av w2 distance
    let w4 = td w3 90
    let w5 = av w4 distance
    let w6 = td w5 90
    let w7 = av w6 distance

    let shapes = getShapes w7
-}
   -- let worlds = drawSquare w 20 0
    print $ "test"




    --TODO color random